<?php

class Comment extends DB
{
    public $id;
    public $userId;
    public $userName;
    public $comment;
    public $createAd;

    public function save()
    {
        $stmt = $this->conn->prepare('SELECT username FROM users WHERE id = :userid');
        $stmt->execute(array('userid' => $this->userId));

        $dbUsername = $stmt->fetch(PDO::FETCH_ASSOC);
        // var_dump($this->userName);
        
        if ($dbUsername["username"] == 'unknown') {
            $dbUsername = $this->userName;
        } else {
            $dbUsername = $dbUsername["username"];
        }
        
        $stmt = $this->conn->prepare('INSERT INTO comments(`user_id`, `user_name`, `comment`) VALUES(:user_id, :user_name, :comment)');
        $stmt->execute(array('user_id' => $this->userId, 'user_name' => $dbUsername, 'comment' => $this->comment));
        $this->id = $this->conn->lastInsertId();

        return $this->id;

    }
    
    public function update()
    {
        $stmt = $this->conn->prepare('UPDATE comments SET comment = :comment_edit WHERE id = :id');
	    $stmt->execute(array('id' => $this->id, 'comment_edit' => $this->comment));
        $this->id = $this->conn->lastInsertId();

        return $this->id;
    }

    public function findAll($start, $limit)
    {
        $stmt = $this->conn->prepare("SELECT * FROM comments ORDER BY id DESC LIMIT ".$start.", ".$limit);
        $stmt->execute();
        $comments = [];
        while ($row = $stmt->fetch(PDO::FETCH_LAZY))
        {
            $comments[] = ['id' => $row->id, 'user_id' => $row->user_id, 'comment' => $row->comment, 'created_at' => $row->created_at, 'user_name' => $row->user_name];
            // var_dump($comments);
        }

        return $comments;
    }

    public function actionAjax()
    {
        $startFrom = $_POST['startFrom'];

        $res = $this->conn->prepare("SELECT * FROM `comments` ORDER BY created_at DESC LIMIT {$startFrom}, 3");
        $res->execute();

        $articles = array();
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $articles[] = $row;
        }

        echo json_encode($articles);
        return true;
    }
}
