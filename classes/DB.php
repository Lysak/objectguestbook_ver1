<?php
class DB
{
    protected $conn = null;
    public $_host = HOST;
    public $_dbname = DBNAME;
    public $_user = USER;
    public $_password = PASSWORD;
    public $_error;

    public function __construct()
    {
        $dns = "mysql:host=".$this->_host.";dbname=".$this->_dbname.";charset=utf8";
        try {
            $this->conn = new PDO($dns, $this->_user, $this->_password);
        } catch (PDOException $e) {
            $this->conn = null;
            $this->_error = $e->getMessage();
        }
    }

    public function getError()
    {
        return $this->_error;
    }
}
