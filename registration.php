<?php
require_once("config.php");
if (!empty($_SESSION['user_id'])) {
    header("location: /index.php");
}
$errors = [];
if (!empty($_POST)) {
    if (empty($_POST['user_name'])) {
        $errors[] = 'Please enter User Name';
    }
    if (empty($_POST['email'])) {
        $errors[] = 'Please enter email';
    }
    if (empty($_POST['first_name'])) {
        $errors[] = 'Please enter First Name';
    }
    if (empty($_POST['last_name'])) {
        $errors[] = 'Please enter Last Name';
    }
    if (empty($_POST['password'])) {
        $errors[] = 'Please enter password';
    }
    if (empty($_POST['confirm_password'])) {
        $errors[] = 'Please enter confirm password';
    }
    if (strlen($_POST['user_name']) > 100) {
        $errors[] = 'User name is too long. Max length is 100 characters';
    }
    if (strlen($_POST['first_name']) > 80) {
        $errors[] = 'First name is too long. Max length is 80 characters';
    }
    if (strlen($_POST['last_name']) > 150) {
        $errors[] ='Last name is too long. Max length is 150 characters';
    }
    if (strlen($_POST['password']) < 6) {
        $errors[] = 'Password should contain at least 6 characters';
    }
    if ($_POST['password'] !== $_POST['confirm_password']) {
        $errors[] = 'Your confirm password is not match password';
    }
    if (empty($errors)) {
        $user = new User();
        $user->userName = $_POST['user_name'];
        $user->email = $_POST['email'];
        $user->password = sha1($_POST['password'].SALT);
        $user->firstName = $_POST['first_name'];
        $user->lastName = $_POST['last_name'];
        $user->save();
        header("location: /login.php?registration=1");
    
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Guest Book</title>
    <style>
        body{ text-align: -webkit-center; }
        #comments-header{ text-align: center; }
        #comments-form{ border: 1px dotted black; width: 50%; padding-left: 20px; }
        #comments-form textarea{ width: 70%; min-height: 100px; }
        #comments-panel{ border: 1px dashed black; width: 50%; padding: 0 10px; margin-top: 20px; }
        .comment-date{ font-style: italic; }
    </style>
    <link href="/template/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <a href="/index.php">GuestBook</a>
    <a href="/login.php">Login</a>
    <a href="/registration.php">Registration</a>
    <a href="/index.php?logout=1">Logout</a>
</div>
<h1>Registration Page</h1>
<div>
    <form method="POST">
    <div style="color: red;">
        <?php foreach ($errors as $error) :?>
            <p><?php echo $error; ?></p>
        <?php endforeach; ?>
    </div>
    <div>
        <label>User name:</label>
        <div>
            <input type="text" name="user_name" required="" value="<?php echo (!empty($_POST['user_name']) ? $_POST['user_name'] : '');?>"/>
        </div>
    </div>
    <div>
        <label>Email:</label>
        <div>
            <input type="text" name="email" required="" value="<?php echo (!empty($_POST['email']) ? $_POST['email'] : '');?>"/>
        </div>
    </div>
    <div>
        <label>First Name:</label>
        <div>
            <input type="text" name="first_name" required="" value="<?php echo (!empty($_POST['first_name']) ? $_POST['first_name'] : '');?>"/>
        </div>
    </div>
    <div>
        <label>Last Name:</label>
        <div>
            <input type="text" name="last_name" required="" value="<?php echo (!empty($_POST['last_name']) ? $_POST['last_name'] : '');?>"/>
        </div>
    </div>
    <div>
        <label>Password:</label>
        <div>
            <input type="password" name="password" required="" value=""/>
        </div>
    </div>
    <div>
        <label>Confirm Password:</label>
        <div>
            <input type="password" name="confirm_password" required="" value=""/>
        </div>
    </div>
    <div>
        <br>
        <input type="submit" value="Register"/>
    </div>
    </form>
</div>
</body>
</html>
