<?php
require_once("config.php");
if (!empty($_SESSION['user_id'])) {
    header("location: /index.php");
}
$errors = [];
$idRegistered = 0;
if (!empty($_GET['registration'])) {
    $idRegistered = 1;
}
if (!empty($_POST)) {
    if (empty($_POST['user_name'])) {
        $errors[] = 'Please enter User Name / Email';
    }
    if (empty($_POST['password'])) {
        $errors[] = 'Please enter password';
    }
    if (empty($errors)) {
        $user = new User();
        $user = $user->checkLogin($_POST['user_name'], sha1($_POST['password'].SALT));
        if (!empty($user->id)) {
            $_SESSION['user_id'] = $user->id;
            header("location: /index.php");
        } else {
            $errors[] = 'Please enter valid credentials';
        }

        /*$stmt = $dbConn->prepare('SELECT id FROM users WHERE (username = :username or email = :username) and password = :password');
        $stmt->execute(array('username' => $_POST['user_name'], 'password' => sha1($_POST['password'].SALT)));
        $id = $stmt->fetchColumn();
        if (!empty($id)) {
            $_SESSION['user_id'] = $id;
            header("location: /index.php");
            // die("Вы успешно авторизированны");
        } else {
            $errors[] = 'Please enter valid credentials';
        }*/
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Guest Book</title>
    <style>
        body{ text-align: -webkit-center; }
        #comments-header{ text-align: center; }
        #comments-form{ border: 1px dotted black; width: 50%; padding-left: 20px; }
        #comments-form textarea{ width: 70%; min-height: 100px; }
        #comments-panel{ border: 1px dashed black; width: 50%; padding: 0 10px; margin-top: 20px; }
        .comment-date{ font-style: italic; }
    </style>
    <link href="/template/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <a href="/index.php">GuestBook</a>
    <a href="/login.php">Login</a>
    <a href="/registration.php">Registration</a>
    <a href="/index.php?logout=1">Logout</a>
</div>
<?php if (!empty($idRegistered)) :?>
    <h2>Вы успешно зарегистрировались! 
        Используйте свои данные для входа на сайт</h2>
<?php endif; ?>
<h1>Log In Page</h1>
<div>
    <form method="POST">
        <div style="color: red;">
            <?php foreach ($errors as $error) :?>
                <p><?php echo $error; ?></p>
            <?php endforeach; ?>
        </div>
        <div>
            <label>User Name / Email:</label>
            <div>
                <input type="text" name="user_name" required="" value="<?php echo (!empty($_POST['user_name']) ? $_POST['user_name'] : '');?>"/>
            </div>
        </div>
        <div>
            <label>Password:</label>
            <div>
                <input type="password" name="password" required="" value="">
            </div>
        </div>
        <div>
            <br>
            <input type="submit" name="submit" value="Log In">
        </div>
    </form>
</div>
</body>
</html>