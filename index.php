<?php

require_once("config.php");



if (isset($_SESSION['user_id'])) {
	$default = $_SESSION['user_id'];
	$myCommentIsBold = $_SESSION['user_id'];
} else {
	$default = '1';
	$myCommentIsBold = '-1';
}

if (isset($_SESSION['user_id'])) {
    $user = $_SESSION['user_id'];
} elseif (isset($_POST['unknownUser'])){
    $user = $_POST['unknownUser'];
} else {
	$user = '1';
}


$comment = new Comment();
if (!empty($_POST['comment'])) {
    $comment->comment = $_POST['comment'];
    $comment->userId = $default;
    $comment->userName = $user;
    $comment->save();
}

if (isset($_GET['comment-edit']) && isset($_GET['comment-id'])) {
    $edit_comment = $_GET['comment-edit'];
    $id_comment = $_GET['comment-id'];
    $comment->comment = $edit_comment;
    $comment->id = $id_comment;
    $comment->update();
}
//  echo '<br>';
//  var_dump($comment);

$comments = $comment->findAll(0,3);

$isLogout = 0;
if (!empty($_GET['logout'])) {
    $isLogout = 1;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments Page</title>
    <style>
        body{ text-align: -webkit-center; }
        #comments-header{ text-align: center; }
        #comments-form{ border: 1px dotted black; width: 50%; padding-left: 20px; }
        #comments-form textarea{ width: 70%; min-height: 100px; }
        #comments-panel{ border: 1px dashed black; width: 50%; padding: 0 10px; margin-top: 20px; }
        .comment-date{ font-style: italic; }
    </style>
    <link href="/template/css/style.css" rel="stylesheet">
</head>
<body>
    <div class="menu">
        <a href="/index.php">GuestBook</a>
        <a href="/login.php">Login</a>
        <a href="/registration.php">Registration</a>
        <a href="/index.php?logout=1">Logout</a>
    </div>
    <?php if (!empty($isLogout)) :?>
        <?php unset($_SESSION["user_id"]); ?>
        <h2>Вы успешно вышли. Приходите ещё!</h2>
    <?php endif; ?>
    <div id="comments-header">
        <h1>Comments Page</h1>
    </div>
    <div id="comments-form">
        <h3>Please add your comment</h3>
        <form method="POST">
            <div>
                <?php 
                    if (!isset($_SESSION['user_id'])) { ?>
                        <label>Username</label>
                        <div>
                            <input name="unknownUser">
                        </div>
                    <?php } ?>
                <label>Comment</label>
                <div>
                    <textarea id="minmessage" name="comment"></textarea>
                </div>
            </div>
            <div>
                <br>
                <input id="submit" type="submit" name="submit" value="Save">
            </div>
        </form>
    </div>
    <div id="comments-panel">
        <h3>Comments:</h3>
        <?php foreach ($comments as $comment) :?>
            <div class="comment-ajax" style="height: 400px; border: 1px solid gray; margin-bottom: 10px;">
                <form method="GET">
                <div <?php if ($comment['user_id'] == $myCommentIsBold) echo 'style="font-weight: bold;"'?> >
                    <p><?= 'User: ' . $comment['user_name'];?></p>
                    <p id="comment" onclick="$(this).hide(); $('.comment_edit<?= $comment['id']; ?>').show();"><?= 'Comment: ' . $comment['comment'];?></p>
                    <input type="text" class="comment_edit<?= $comment['id']; ?>" value="<?= $comment['comment'];?>" name="comment-edit" style="display: none;">
                    <input type="text" name="comment-id" value="<?= $comment['id']; ?>" style="display: none;">
                    <p><?php echo 'at: '; ?>
                    <span class="comment-date">(<?php echo $comment['created_at'];?>)</span></p>
                   
                    <input id="submit" type="submit" name="submit" value="EDIT">
                </div>
                </form>
            </div>
        <?php endforeach; ?>
    </div>
</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="/template/js/ajax.js"></script>
<script type="text/javascript" src="/template/js/script.js"></script>
</html>